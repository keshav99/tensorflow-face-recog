import cv2
import numpy as np

def cam():
    cam = cv2.VideoCapture(0)

    img_counter = 0

    while True:
        ret, frame = cam.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        # frame = cv2.adaptiveThreshold(frame, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 115,1)
        lower_blue = np.array([50,0,5])
        upper_blue = np.array([100,255,100])

        mask =  cv2.inRange(frame, lower_blue, upper_blue)
        res = cv2.bitwise_and(frame, frame, mask = mask)

        kernel = np.ones((5,5), np.uint8)
        erosion = cv2.erode(mask, kernel, iterations =1)
        dilation = cv2.dilate(mask, kernel, iterations =1)

        opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
        closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        

        cv2.imshow('frame', opening)
        cv2.imshow('frame2', closing)
        cv2.imshow('res', res)
        key = cv2.waitKey(1) & 0xFF

        if key == ord("q"):
                break

    cam.release()
    cv2.destroyAllWindows()
