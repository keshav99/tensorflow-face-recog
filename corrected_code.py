import tensorflow as tf
import numpy as np
from . import facenet
from . import detect_face
import cv2
import os
import argparse
import imutils
import sys
import time
from imutils.video import FPS



minsize = 20
threshold = [0.6, 0.7, 0.7]
factor = 0.90
margin = 44
input_image_size = 160
sess = tf.Session()

facenet.load_model( os.path.dirname(__file__) + "/static/20170512-110547/20170512-110547.pb")

# Get input and output tensors



# read pnet, rnet, onet models from align directory and files are det1.npy, det2.npy, det3.npy
pnet, rnet, onet = detect_face.create_mtcnn(sess,  os.path.dirname(__file__) +'/align')


# read 20170512-110547 model file downloaded from https://drive.google.com/file/d/0B5MzpY9kBtDVZ2RpVDYwWmxoSUk
images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
embedding_size = embeddings.get_shape()[1]
def face_detect(args2, file, name):
    
    if(args2=='save'):
        return save(file, name)
    else:
        return find(file, name)
    
    # parser.add_argument("--img2", type = str, required=True)
    # args = parser.parse_args()

    # some constants kept as default from facenet
    

def getFace(img):
    faces = []
    img_size = np.asarray(img.shape)[0:2]
    bounding_boxes, _ = detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
    if not len(bounding_boxes) == 0:
        for face in bounding_boxes:
            if face[4] > 0.50:
                det = np.squeeze(face[0:4])
                bb = np.zeros(4, dtype=np.int32)
                bb[0] = np.maximum(det[0] - margin / 2, 0)
                bb[1] = np.maximum(det[1] - margin / 2, 0)
                bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
                bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
                cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
                resized = cv2.resize(cropped, (input_image_size,input_image_size),interpolation=cv2.INTER_CUBIC)
                prewhitened = facenet.prewhiten(resized)
                faces.append({'face':resized,'rect':[bb[0],bb[1],bb[2],bb[3]],'embedding':getEmbedding(prewhitened)})
    return faces
def getEmbedding(resized):
    reshaped = resized.reshape(-1,input_image_size,input_image_size,3)
    feed_dict = {images_placeholder: reshaped, phase_train_placeholder: False}
    embedding = sess.run(embeddings, feed_dict=feed_dict)
    return embedding

def compare2face(img1,img2):
    face1 = getFace(img1)
    face2 = getFace(img2)
    if face1 and face2:
        # calculate Euclidean distance
        dist = np.sqrt(np.sum(np.square(np.subtract(face1[0]['embedding'], face2[0]['embedding']))))
        return dist
    return -1

new_path = os.path.dirname(__file__) + '/static/tf_images/'
new_folder = os.listdir(new_path)

def save(img1, name):
    #name = input('Enter your name: ')
    img1  = cv2.imread(img1)
    img1 = imutils.resize(img1,width=1000)
    faces = getFace(img1)
    for face in faces:
            cv2.rectangle(img1, (face['rect'][0], face['rect'][1]), (face['rect'][2], face['rect'][3]), (0, 255, 0), 2)
    # cv2.imshow("faces", img1)
    key = cv2.waitKey(1) & 0xFF
    # if key == ord("q"):
    path, dirs, files = next(os.walk(os.path.dirname(__file__) + "/static/tf_images/"+name))           #  CHANGE HERE
    file_count = len(files)           #  CHANGE HERE
    cv2.imwrite(os.path.dirname(__file__) + "/static/tf_images/"+name+"/"+name+file_count+".jpg", img1)           #  CHANGE HERE
    print("Saved")
    return "Saved"
    


def find(img1):
    # files = os.listdir(train_path)
    trained_path = os.path.dirname(__file__) + "/static/tf_images/"
    img2 = [0 for i in range(30)]
    dist = [0 for i in range(30)]
    img1 = cv2.imread(img1)
   
    img1 = imutils.resize(img1,width=1000)
    faces = getFace(img1)
    # cv2.imshow("faces", img1)
    k = 0
    for i in new_folder:
        # img1[i] = cv2.imread(os.path.join(train_path, str(i+1)+'.jpg'))
        for j in i:
            img2[k] = cv2.imread(os.path.join(trained_path, i+"/"+j))           #  CHANGE HERE
            k+=1

    smallest = 1.10
    correct = -1
    if(len(faces)>0):
        for j in range(len(new_folder)):
            comp = compare2face(img1, img2[j])
            # print(comp)
            if(comp<smallest and comp!=-1):
                correct = j
                smallest = comp
        if correct!=-1:
            for face in faces:
                cv2.rectangle(img1, (face['rect'][0], face['rect'][1]), (face['rect'][2], face['rect'][3]), (0, 255, 0), 2)
                cv2.putText(img1, new_folder[correct][0:-3]+' Distance: '+str(smallest), (face['rect'][0], face['rect'][1]), cv2.FONT_HERSHEY_TRIPLEX,1,(255,255,255),1,cv2.LINE_AA)  
                print("Hello "+new_folder[correct][0:-3])
                return (new_folder[correct][0:-3]+' '+str(100-(smallest*100/1.4)))           #  CHANGE HERE
               
    
